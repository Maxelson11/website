'use strict'

require('dotenv').config()
const production = process.env.NODE_ENV || 'dev';
const { DefinePlugin, EnvironmentPlugin } = require('webpack')
const webpack = require('webpack')
const HTMLPlugin = require('html-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')
const UglifyPlugin = require('uglifyjs-webpack-plugin')
const ExtractPlugin = require('extract-text-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin');
const path = require('path');


const ASSET_PATH = process.env.ASSET_PATH || '/'


let plugins = [
  new EnvironmentPlugin({NODE_ENV:'production', DEBUG:false}),
  new ExtractPlugin('bundle-[hash].css'),
  new HTMLPlugin({ template: `${__dirname}/front-end/src/index.html` }),
  new DefinePlugin({
    'process.env': {
      __PORT__: JSON.stringify(process.env.PORT),
      DEBUG: JSON.stringify(!production),
      APP_ID: JSON.stringify(process.env.APP_ID),
      APP_SECRET: JSON.stringify(process.env.APP_SECRET),
      CALLBACK_URL: JSON.stringify(process.env.CALLBACK_URL),
      __TEST__: JSON.stringify(process.env.testing),
      __RESUME__: JSON.stringify(process.env.resume)
    }
  }),
  new webpack.optimize.AggressiveMergingPlugin(),
  new CompressionPlugin({
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8
    })
]


console.log('this is our production thing!!!!', process.env.NODE_ENV);

if (production=== 'production')
  plugins = plugins.concat([
    new CleanPlugin(),
    new UglifyPlugin(),
    new EnvironmentPlugin({NODE_ENV:'production', DEBUG:false}),
    new webpack.optimize.AggressiveMergingPlugin(),
    new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8
      })
])

module.exports = {
  plugins,
  entry: `${__dirname}/front-end/src/main.js`,
  devServer: {
    historyApiFallback: true,
  },
  devtool: production ? undefined : 'cheap-module-eval-source-map',
  output: {
    path: `${__dirname}/build`,
    filename: 'bundle-[hash].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.es6$/,
        exclude: /node_modules/,
        loader: 'babel',
      },
      {
        test: /\.scss$/,
        loader: ExtractPlugin.extract(['css-loader', 'sass-loader']),
      },
      {
        test: /\.icon.svg$/,
        loader: 'raw-loader',
      },
      {
        test: /\.(woff|woff2|ttf|eot).*/,
        exclude: /\.icon.svg/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'font/[name].[hash].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(pdf|jpg|jpeg|gif|png|tiff|svg)$/,
        exclude: /\.icon.svg$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 60000,
              name: '[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(mp3|aac|aiff|wav|flac|m4a|ogg)$/,
        exclude: /\.glyph.svg/,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'audio/[name].[ext]' },
          },
        ],
      },
    ],
  },
}
