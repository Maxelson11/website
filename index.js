const express = require('express')
const morgan = require('morgan')

const app = express()

app.use(express.static(`${__dirname}/build`))
app.get('*.min.js', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  next();
})


app.listen(process.env.PORT || 3000, () => {
  console.log('__SERVER_RUNNING__', process.env.PORT || 3000)
})
