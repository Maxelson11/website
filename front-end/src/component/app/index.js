
import React from 'react'
import {BrowserRouter, Route, Link, Switch, MemoryRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import LandingContainer from '../landing-container/index.js'

import appStoreCreate from '../../lib/app-store-create.js'


const store = appStoreCreate()


class App extends React.Component {
  constructor(props){
    super(props)
  }


  componentDidMount(){
    store.subscribe(()=> {
  })
  store.dispatch({type:null})
  }

  render() {

      return(
        <div className='app'>
        <BrowserRouter>
        <div>
        <Route exact path="/" component={LandingContainer}/>
        </div>
        </BrowserRouter>
        </div>
      )
  }
}

let mapStateToProps = (state) => ({
  })

let mapDispatchToProps = (dispatch) => ({
})


export default connect(mapStateToProps, mapDispatchToProps)(App)
