import './_projects.scss'
import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'



class ProjectForm extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <a className={this.props.className} href={this.props.href}>
        <div>
        <p id="project-title" className="standard-text">{this.props.title} </p>
        <p className="project-description">{this.props.description}</p>
        <div className="project-skills-container">
        {this.props.skills.map((skill, i)=>{
          return <div className="skill" key={i}>{skill}</div>
        })}
        </div>
        </div>
      </a>
    )
  }
}

ProjectForm.propTypes = {
  title: React.PropTypes.string,
  description: React.PropTypes.string,
  href: React.PropTypes.string,
  className: React.PropTypes.string,
  skills: React.PropTypes.array,
}



export default ProjectForm
