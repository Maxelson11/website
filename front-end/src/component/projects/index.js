import './_projects.scss'
import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
// import Unsplash from 'Unsplash-js'
import ProjectForm from './project-form.js'



class Projects extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className="section-container" id="projects-container">
      <p className="section-title"> Open Source Portfolio </p>

        <ProjectForm
		className="project-container"
		title='Poller'
		description="Web application allowing users to create an account, submit 
		profile demographic information, post and vote on yes or no questions,
		and see the results anonymously."
		href="https://poller-eefdf.firebaseapp.com/"
		skills={['Oauth', "React", "Material-ui",'Heroku', 'Postgresql','NodeJS']}
        />


      </div>
    )
  }
}

export default Projects
