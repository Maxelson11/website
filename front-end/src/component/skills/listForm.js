import './_skills.scss'
import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import FontAwesome from 'react-fontawesome'



class ListForm extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
        <ul className={"skills-list"} id={this.props.id}>
        <p id={"skills-list-title"}>
          <FontAwesome
          className="skills-title-icon"
          name={this.props.iconName}
          size="lg"
          style={{display:"inline"}}
          />
          {this.props.listName}
        </p>
        {this.props.skills.map((skill, i)=>{
          return <li className="list-skill" key={i}>{skill}</li>
        })}
        </ul>

    )
  }
}

ListForm.propTypes = {
  listName: React.PropTypes.string,
  iconName: React.PropTypes.string,
  skills: React.PropTypes.array,
}

export default ListForm
