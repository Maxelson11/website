import './_skills.scss'
import React from 'react'
import ListForm from './listForm.js'
import ReactIcon from './react_icon.js'
import MochaIcon from './mocha_icon.js'
import BabelIcon from './babel_icon.js'
import BootstrapIcon from './bootstrap_icon.js'
import CssIcon from './css_icon.js'
import HerokuIcon from './heroku_icon.js'
import HtmlIcon from './html_icon.js'
import MongoIcon from './mongodb_icon.js'
import NodeIcon from './nodejs.js'
import SassIcon from './sass_icon.js'
import VueIcon from './vue_icon.js'
import WebpackIcon from './webpack_icon.js'

class Skills extends React.Component{
  constructor(props){
    super(props)
    this.state = {

    }
  }

  render(){
    return(
      <div id="skills-page" className="section-container">
      <div id="skills-container">
        <p className="section-title">Skills</p>
        <div id="fullstack-section">
          <ListForm
          id="full-stack-list"
          listName="Full-Stack JavaScript Development"
          iconName="pencil-square-o"
          skills={['Functional Programming','Continuous Integration','OOP','Higher Order Functions','ES6', 'Basic/Bearer Auth','AJAX','JSON']}
          />
        </div>
        <div id="front-end-section">
          <ListForm
          id="front-end-list"
          listName="Front-End Development"
          iconName="paint-brush"
          skills={['React','React-Native','Jest','Structural Design Patterns','Async Design Patterns']}
          />
        </div>
        <div id="back-end-section">
        <ListForm
        id="back-end-list"
        listName="Back-End Development"
        iconName="database"
        skills={['NodeJS','MongoDB','AWS S3','PostGresSQL','RESTful API','Data Modeling','TCP & HTTP','Mocha & Chai Testing']}
        />
        </div>
        <div id="dev-ops-section">
        <ListForm
        id="dev-ops-list"
        listName="Dev-Ops, UX/UI"
        iconName="cogs"
        skills={['Heroku','Travis CI','Git + Github','Debugging','Behavior-Driven Development','Unit, Integration, & End-to-End testing','ESlint','Chrome DevTools']}
        />
        </div>
        <div className="tools-container">
        <HtmlIcon/>
        <ReactIcon />
        <BootstrapIcon/>
        <CssIcon/>
        <BabelIcon/>
        <HerokuIcon/>
        <MongoIcon/>
        <NodeIcon/>
        <SassIcon/>
        <MochaIcon/>
        <VueIcon/>
        <WebpackIcon/>
        </div>
      </div>
    </div>
    )
  }
}

export default Skills
