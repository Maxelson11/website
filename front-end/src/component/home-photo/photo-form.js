import './_home-photo.scss'
import './_photo-form.scss'
import React from 'react'
import Projects from '../projects/index.js'


class PhotoForm extends React.Component{
  constructor(props){
    super(props)
  }

  render(){
    return(
      <div >
        <Projects/>
        {/* <div id="unsplash-credit-container">
        <div id="unsplash-credit-text">
        <a
        href={this.props.href}>Photo by: {this.props.author}/ </a>
        <a
        href="https://unsplash.com"> Unsplash </a>
        </div>
        </div> */}
        <img className="image" src={this.props.image} />
      </div>
    )
  }
}


PhotoForm.propTypes = {
  title: React.PropTypes.string,
  description: React.PropTypes.string,
  href: React.PropTypes.string,
  className: React.PropTypes.string,
  skills: React.PropTypes.array,
}

export default PhotoForm
