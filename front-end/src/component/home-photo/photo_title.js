import './_home-photo.scss'
import React from 'react'
import {connect} from 'react-redux'


class PhotoTitle extends React.Component {
  constructor(){
    super()
    this.state = {
      timer: null,
      index:0,
      wordBank:['Software Engineer', 'Michael Axelson', 'Whitman College class \'14', 'Economics Student', 'Spanish/Italian Speaker'],
    }
    this.updateIndex = this.updateIndex.bind(this)

  }
  componentWillMount(){

  }
  componentWillUnmount(){
    clearInterval(this.state.timer)
    clearInterval(this.state.index)
  }

  updateIndex(){
        let random = Math.floor(Math.random()*this.state.wordBank.length)
        this.setState({index:random})
    }


  render() {
    return(
      <p id="intro-text"> Software Engineer </p>
    )
  }
}


export default PhotoTitle
