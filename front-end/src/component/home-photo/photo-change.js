import './_home-photo.scss'
import React from 'react'
import {connect} from 'react-redux'
import PhotoTitle from './photo_title.js'
import FontAwesome from 'react-fontawesome'
import Scroll from 'react-scroll'
import PhotoForm from './photo-form.js'
import { Parallax } from 'react-scroll-parallax'


const Link = Scroll.Link;

class PhotoChange extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      timer: null,
      imageNumber1: 0,
      imageNumber2: 1,
      imageNumber3: 2,
      imageNumber4: 3,
      imageNumber5: 4,
      photos: this.props.props.photos,
      authors: this.props.props.authors,
      authorLinks: this.props.props.authorLinks,
      locations: this.props.props.locations,
    }
    this.updateImages = this.updateImages.bind(this)
  }

  componentDidMount(){
    let timer = setInterval(this.updateImages, 10000)
    this.setState({timer})
  }

  updateImages(){
        let random1 = Math.floor(Math.random()*this.state.photos.length)
        let random2 = Math.floor(Math.random()*this.state.photos.length)
        let random3 = Math.floor(Math.random()*this.state.photos.length)
        let random4 = Math.floor(Math.random()*this.state.photos.length)
        this.setState({imageNumber1:random1, imageNumber2:random2, imageNumber3: random3, imageNumber4:random4})
    }

    componentWillUnmount(){
      clearInterval(this.state.imageNumber1)
      clearInterval(this.state.imageNumber2)
      clearInterval(this.state.imageNumber3)
      clearInterval(this.state.imageNumber4)
      clearInterval(this.state.timer)
    }


  render(){

    const {locations, authorLinks, photos, authors, imageNumber1, imageNumber2,imageNumber3,imageNumber4} = this.state;

    return (
        <div id="photo-container">
        <PhotoForm
        image={photos[imageNumber1]}
        href={authorLinks[imageNumber1]}
        location={locations[imageNumber1]}
        authorLink={authorLinks[imageNumber1]}
        author={authors[imageNumber1]}
        />
        </div>
    )
  }
}

let mapStateToProps = (state) => ({
})

let mapDispatchToProps = (dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(PhotoChange)
