import './_home-photo.scss'
import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import Unsplash, {toJson} from 'unsplash-js'
import {isLoadingAction, authorsAction, authorLinksAction, locationsAction, photosAction} from '../../actions/unsplash-photo.js'
import * as util from '../../lib/util.js'
import PhotoChange from './photo-change.js'
import PhotoTitle from './photo_title.js'
import { Parallax } from 'react-scroll-parallax'

class HomePhoto extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      result: null,
      isLoading: true,
      photos: [],
      authors: [],
      authorLinks: [],
      locations: [],
    }
    this.handleGetPhotos = this.handleGetPhotos.bind(this)
    this.photosToProps = this.photosToProps.bind(this)
  }

  photosToProps(photos, authors, authorLinks, locations) {
    this.props.getPhotos(photos);
    this.props.getAuthors(authors);
    this.props.getAuthorLinks(authorLinks);
    this.props.getLocations(locations);
    this.props.isLoadingChange()
  }

  handleGetPhotos(){
    const unsplash = new Unsplash({
      applicationId: `${process.env.APP_ID}`,
      secret: `${process.env.APP_SECRET}`,
      // callbackUrl: `${process.env.CALLBACK_URL}`,
    })
      return unsplash.photos.getRandomPhoto({
        featured: true,
        width: 400,
        height: 500,
        count:30,
      })
      .then(toJson)
      .then(res => {
        let photos = []
        let authors = []
        let authorLinks = []
        let locations = []
        res.map(item=>{
          let photo = item.urls.regular
          let author = item.user.first_name+" "+item.user.last_name
          let authorLink = item.links.html
          let location = item.user.location
          photos.push(photo)
          authors.push(author)
          authorLinks.push(authorLink)
          if (location) {
            locations.push(location)
          }else {
            locations.push('')
          }
        })
        let isLoading = false;
        this.setState({photos, authors, authorLinks, locations})
        this.props.getPhotos(photos);
        this.props.getAuthors(authors);
        this.props.getAuthorLinks(authorLinks);
        this.props.getLocations(locations);
        this.props.isLoadingChange()

      })
      .catch(err=>{
        util.logError
      
      })

  }
  componentWillMount(){
    if (this.props.isLoading)
      this.handleGetPhotos()
    return null
}

  render(){
    if (this.props.isLoading) {
      return (
        <div id="waiting-container">
          <p id="waiting-text"> Hello ! </p>
        </div>
    )
    } else {
      return (
        <div id="portfolio-container">    
            <PhotoChange props={this.props}/>
        </div>
      )
    }
  }
}

let mapStateToProps = (state) => ({
  photos: state.photos,
  authors: state.authors,
  authorLinks: state.authorLinks,
  locations: state.locations,
  isLoading: state.isLoading,
})

let mapDispatchToProps = (dispatch) => {

  return {
    getPhotos: (photos)=> dispatch(photosAction(photos)),
    getLocations: (locations) => dispatch(locationsAction(locations)),
    getAuthorLinks: (authorLinks) => dispatch(authorLinksAction(authorLinks)),
    getAuthors: (authors) => dispatch(authorsAction(authors)),
    isLoadingChange: () => dispatch(isLoadingAction()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePhoto)
