import './_landing-container.scss'
import React from 'react'
import NavBar from '../navbar/index.js'
import HomePhoto from '../home-photo/api-photo.js'
import Projects from '../projects/index.js'
import Contact from '../contact/contact.js'
import {SectionDivide} from  '../navbar/divider.js'


class LandingContainer extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div id="app-container">
      <p id="title-name" > Michael Axelson </p>
      <NavBar/>
      <Contact  />
	   <SectionDivide/>
      <Projects /> 
      </div>
    )
  }
}

export default LandingContainer
