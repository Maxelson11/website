import './_navbar.scss'
import './_modal.scss'
import React from 'react'

import {connect} from 'react-redux'

import FontAwesome from 'react-fontawesome'

// import * as util from '../../lib/util.js'
import Scroll from 'react-scroll'

const Link       = Scroll.Link;
const Element    = Scroll.Element;
const Events     = Scroll.Events;
const scroll     = Scroll.animateScroll;
const scrollSpy  = Scroll.scrollSpy;


class Modal extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div id="modal">
        <div id="modal-categories">
          <Link activeClass="active" to="unsplash-credit-container" spy={true} smooth={true} duration={500}>
            <p className="modal-name" onClick={this.props.handleClickDropDown}> Home </p>
            <div className="modal-button"></div>
          </Link>
          <Link activeClass="active" to="introduction-page" spy={true} smooth={true} duration={500}>
            <p className="modal-name" onClick={this.props.handleClickDropDown}> Intro </p>
            <div className="modal-button"></div>
          </Link>
          <Link activeClass="active" to="skills-page" spy={true} smooth={true} duration={500}>
            <p className="modal-name" onClick={this.props.handleClickDropDown}>Skills</p>
            <div className="modal-button"></div>
          </Link>
          <Link activeClass="active" to="projects-page" spy={true} smooth={true} duration={500} id="navbar-size">
            <p className="modal-name" onClick={this.props.handleClickDropDown}>Portfolio</p>
            <div className="modal-button"></div>
          </Link>
          <Link activeClass="active" to="contact-page" spy={true} smooth={true} duration={500}>
            <p className="modal-name" onClick={this.props.handleClickDropDown}> Contact </p>
            <div className="modal-button"></div>
          </Link>
        </div>
      </div>
    )
  }
}

export default Modal
