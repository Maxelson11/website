import './_navbar.scss'
import React from 'react'
import {connect} from 'react-redux'
import FontAwesome from 'react-fontawesome'
import Modal from './modal.js'
import Scroll from 'react-scroll'


const Link       = Scroll.Link;
const Element    = Scroll.Element;
const Events     = Scroll.Events;
const scroll     = Scroll.animateScroll;
const scrollSpy  = Scroll.scrollSpy;



class NavBar extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      dropDownOpen: false,
    }
    this.handleClickDropDown= this.handleClickDropDown.bind(this)
  }
    handleClickDropDown() {
    const currentState = this.state.dropDownOpen
    this.setState({dropDownOpen: !currentState});
  }
  render() {
    return(
      <div className='nav-bar'>
        <div>
          <div id="nav-bar-categories">
	    <Link activeClass="active" to="contact-page" spy={true} smooth={true}  duration={250}>
              <p className="category-name"> Contact </p>
              <div className="category-button"></div>
            </Link>

            <Link activeClass="active" to="projects-container" spy={true} smooth={true}  duration={250}>
              <p className="category-name"> Portfolio </p>
              <div className="category-button"></div>
            </Link>
          </div>

          <p id="name"> Michael Axelson </p>
        </div>

      </div>
    )
  }
}

let mapStateToProps = (state) => ({
  modalClosed: state.modalClosed
})

let mapDispatchToProps = (dispatch) => ({
})


export default connect(mapStateToProps, mapDispatchToProps)(NavBar)
