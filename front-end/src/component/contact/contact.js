import './_contact.scss'
import React from 'react'
import intro_photo from "./intro_photo.jpg"
import FontAwesome from 'react-fontawesome'
import EnglishResume from '../../pictures/Resume.pdf'



class Contact extends React.Component{
  constructor(props){
    super(props)
    this.state = {

    }
  }

  render(){
    return(
      <div id="contact-page" className="section-container">
        <p className="section-title"> Contact </p>
          <div >
            <img id="intro-photo" src={intro_photo}/>


            <div id="contact-information">


              <div className="contact-button">
                <a href={EnglishResume}>
                  <p className="standard-text">Resume- English</p>
                </a>
              </div>

              <p className="standard-text"> Location: Seattle, Wa </p>
              <p className="standard-text"> Work: 206-794-5265</p>

            </div>
            <div id="contact-buttons">
              <a href="mailto:axelmichael11@gmail.com">
                <FontAwesome
                name='envelope-o'
                size="2x"
                />
              </a>
              <a href="https://www.linkedin.com/in/michael-axelson-80620389/">
                <FontAwesome
                name='linkedin-square'
                size="2x"
                />
              </a>
              <a href="https://github.com/axelmichael11">
                <FontAwesome
                name='github'
                size="2x"
                />
              </a>
              <a href="https://www.twitter.com/maxelson11">
                <FontAwesome
                  name='twitter-square'
                  size="2x"
                />
              </a>
            </div>
          </div>
      </div>
    )
  }
}

export default Contact
