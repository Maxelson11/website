import Unsplash, {toJson} from 'unsplash-js'
import * as util from '../lib/util.js'

export const photosAction = (photos) => {
  return {type: 'GET_PHOTOS',
  payload: photos,
  }
}


export const authorsAction = (authors) => {
  return {type: 'GET_AUTHORS',
  payload: authors,
  }
}

export const authorLinksAction = (authorLinks) => {
  return {type: 'GET_AUTHORLINKS',
  payload: authorLinks,
  }
}

export const locationsAction = (locations) => {
  return {type: 'GET_LOCATIONS',
  payload: locations,
  }
}

export const isLoadingAction = () => {
  return {type: 'ISLOADING',
  payload: true,
  }
}
