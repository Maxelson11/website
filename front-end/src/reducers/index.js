import {combineReducers} from 'redux'
import photosReducer from './unsplash-photos.js'
import locationsReducer from './unsplash-locations.js'
import authorsReducer from './unsplash-authors.js'
import authorLinksReducer from './author-links.js'
import isloading from './isloading.js'
export default combineReducers({
  photos: photosReducer,
  locations: locationsReducer,
  authors: authorsReducer,
  authorLinks: authorLinksReducer,
  isLoading: isloading,
})
